/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.oxprogram;

import java.util.Scanner;

/**
 *
 * @author Rattanalak
 */
public class OXProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    private static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTable() {
        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table.length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println(" ");
        }
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row , col:  ");
        row = kb.nextInt();
        col = kb.nextInt();

    }

    public static void process() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                showWin();
                return;
            }
            switchPlayer();
        }
    }

    private static void showWin() {
        showTable();
        if (checkDraw()) {
            System.out.println(">>>Draw<<<");
            return;
        } else {
            System.out.println(">>>" + currentPlayer + " Win<<<");
        }
    }

    public static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean checkWin() {
        if (checkVertical(table, currentPlayer, col)) {
            return true;
        } else if (checkHorizontal(table, currentPlayer, row)) {
            return true;
        } else if (CheckX(table, currentPlayer)) {
            return true;
        } else if (checkDraw()) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical(char[][] table1, char currentPlayer1, int col1) {
        for (int row = 0; row < table.length; row++) {
            if (table[row][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal(char[][] table1, char currentPlayer1, int row1) {
        for (int col = 0; col < table.length; col++) {
            if (table[row - 1][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean CheckX(char[][] table1, char currentPlayer1) {
        if (checkX1(table,currentPlayer)) {
            return true;
        } else if (checkX2(table,currentPlayer)) {
            return true;
        }
        return false;
    }

    private static boolean checkX1(char[][] table1, char currentPlayer1) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2(char[][] table1, char currentPlayer1) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDraw() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

}
